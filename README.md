# Utils

A set of small utilities

----

- ## alarm
    launch a music player to wake me up

- ## fed
    filter editor (or something like that)
    use an editor as a part of a shell pipe

- ## inplace
    modify files in-place with an arbitrary shell command

- ## tt
    a (dumb) time tracker

- ## create-links
    create symlinks specified by links.yaml

- ## pt
    pomodoro method timer

- ## wrk
  keyboard and mouse settings

- ## sw
  switch between workman and querty layouts
